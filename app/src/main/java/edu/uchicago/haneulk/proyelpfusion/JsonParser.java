package edu.uchicago.haneulk.proyelpfusion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JsonParser {

    private static final String USER_AGENT = "Mozilla/5.0";
    private static final String API_KEY = "_tRKH6fj0jsWD8aGkHupcf9WXFAbODx84jAkG5NFTiC4MlZB7IKWBfDoQfsQqn8u-2Lad0rZCcHhaP9gIacpTg_tqk7uYNdLRas79nIgs708ujASyod5NUmkY-PlXHYx";
    public static JSONObject fetchJson(String strUrl) throws Exception {

        URL url = new URL(strUrl);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod("GET");
        // set API_KEY
        httpURLConnection.setRequestProperty("Authorization", "Bearer " + API_KEY);
        httpURLConnection.setRequestProperty("User-Agent", USER_AGENT);
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode == 200){
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getInputStream()));
            String inputLine;
            StringBuffer stringBuffer = new StringBuffer();

            while ((inputLine = bufferedReader.readLine()) != null) {
                stringBuffer.append(inputLine);
            }
            bufferedReader.close();
            try {
                return new JSONObject(stringBuffer.toString());
            } catch (JSONException e) {
                return null;
            }

        } else {
            return null;
        }

    }


}
