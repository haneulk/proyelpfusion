package edu.uchicago.haneulk.proyelpfusion;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

public class ResultActivity extends AppCompatActivity {

    private ArrayList<String> restaurantList = new ArrayList<>();
    public static final String DETAILS = "https://api.yelp.com/v3/businesses/";
    public static final String REVIEWS = "/reviews";
    private TextView name;
    private RatingBar rating;
    private TextView price;
    private TextView phone;
    private TextView address;
    private TextView hours;
    private TextView reviews;
    private Button buttonAgain;
    private Button buttonBack;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_result);

        //unpack ArrayList from the bundle and convert to array
        final ArrayList<String> restaurantList = ((ArrayList<String>)
                getIntent().getSerializableExtra(MainActivity.RESTAURANT_LIST));

        name = (TextView) findViewById(R.id.restaurant_name);
        rating = (RatingBar) findViewById(R.id.restaurant_rating);
        price = (TextView) findViewById(R.id.restaurant_price);
        phone = (TextView) findViewById(R.id.restaurant_phone);
        address = (TextView) findViewById(R.id.restaurant_address);
        hours = (TextView) findViewById(R.id.restaurant_hours);
        reviews = (TextView) findViewById(R.id.restaurant_review);
        buttonAgain = (Button) findViewById(R.id.button_again);
        buttonBack = (Button) findViewById(R.id.button_back);


        overallTask(restaurantList);

        buttonAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overallTask(restaurantList);
            }
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // go back to main activity
                Intent intent = new Intent(ResultActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    // main process of result activity
    private void overallTask(ArrayList<String> restaurantList){
        Random rand = new Random();
        int n = rand.nextInt(restaurantList.size());
        String restaurantID;
        restaurantID = restaurantList.get(n);
        String urlDetails = DETAILS + restaurantID;
        String urlReviews = DETAILS + restaurantID + REVIEWS;
        new FetchDetailsTask().execute(urlDetails);
        new FetchReviewsTask().execute(urlReviews);
    }

    // get details
    private class FetchDetailsTask extends AsyncTask <String, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                return JsonParser.fetchJson(params[0]);
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if (jsonObject == null) {
                    throw new JSONException("no data available.");
                }

                // deal with JSON object, set text for related fields
                name.setText(jsonObject.getString("name"));
                rating.setRating((float)jsonObject.getDouble("rating"));
                price.setText(jsonObject.getString("price"));
                phone.setText(jsonObject.getString("display_phone"));
                String addressText = "";
                JSONArray addressArray = jsonObject.getJSONObject("location").getJSONArray("display_address");
                for (int i=0; i<addressArray.length(); i++){
                    addressText += addressArray.getString(i);
                    if (i != addressArray.length()-1){
                        addressText += "\n";
                    }
                }

                address.setText(addressText);

                // process hours string
                String start = "";
                String end = "";
                start = jsonObject.getJSONArray("hours").getJSONObject(0).getJSONArray("open").getJSONObject(0).getString("start");
                end = jsonObject.getJSONArray("hours").getJSONObject(0).getJSONArray("open").getJSONObject(0).getString("end");
                String hoursText = start.substring(0,2) + ":" + start.substring(2,4) + " - " + end.substring(0,2) + ":" + end.substring(2,4);
                hours.setText(hoursText);


            } catch (JSONException e) {
                Toast.makeText(
                        ResultActivity.this,
                        "There's been a JSON exception: " + e.getMessage(),
                        Toast.LENGTH_LONG
                ).show();
                e.printStackTrace();
                finish();
            }
        }
    }

    // get reviews
    private class FetchReviewsTask extends AsyncTask <String, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                return JsonParser.fetchJson(params[0]);
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if (jsonObject == null) {
                    throw new JSONException("no data available.");
                }

                String reviewText = "";
                JSONArray reviewArray = jsonObject.getJSONArray("reviews");
                for (int i=0; i<reviewArray.length(); i++){
                    JSONObject review = reviewArray.getJSONObject(i);
                    reviewText += review.getString("time_created") + "\n" + review.getString("text") + "\n" + "\n";
                }

                reviews.setText(reviewText);

            } catch (JSONException e) {
                Toast.makeText(
                        ResultActivity.this,
                        "There's been a JSON exception: " + e.getMessage(),
                        Toast.LENGTH_LONG
                ).show();
                e.printStackTrace();
                finish();
            }
        }
    }


}
