package edu.uchicago.haneulk.proyelpfusion;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private EditText mEditTextLocation;
    private Switch mSwitchOpenNow;
    private Switch mSwitchDelivery;
    private Switch mSwitchTakeOut;
    private Spinner mSpinnerPrice;
    private Button mButtonChoose;

    private final String[] mPrices = new String[] {"no selection", "$", "$$", "$$$", "$$$$"};
    public static final String BUSINESS_SEARCH = "https://api.yelp.com/v3/businesses/search?radius=2000";
    public static final String RESTAURANT_LIST = "restaurant_list";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //assign references to our Views
        mEditTextLocation = (EditText) findViewById(R.id.edit_text_location);
        mSwitchOpenNow = (Switch) findViewById(R.id.switch_open_now);
        mSwitchDelivery = (Switch) findViewById(R.id.switch_delivery);
        mSwitchTakeOut = (Switch) findViewById(R.id.switch_take_out);
        mSpinnerPrice = (Spinner) findViewById(R.id.spinner_price);
        mButtonChoose = (Button) findViewById(R.id.button_choose);

        mEditTextLocation.setText(PrefsMgr.getString(this, "Location"));
        mSwitchOpenNow.setChecked(PrefsMgr.getBoolean(this, "OpenNow"));
        mSwitchDelivery.setChecked(PrefsMgr.getBoolean(this, "Delivery"));
        mSwitchTakeOut.setChecked(PrefsMgr.getBoolean(this, "TakeOut"));
        System.out.println(PrefsMgr.getInt(this, "Price"));

        //controller: mediates model and view
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                //context
                this,
                //view: layout you see when the spinner is closed
                R.layout.spinner_closed,
                //model: the array of Strings
                mPrices
        );

        //view: layout you see when the spinner is open
        arrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);

        mSpinnerPrice.setAdapter(arrayAdapter);
        mSpinnerPrice.setSelection(PrefsMgr.getInt(this, "Price"));

        //click listener for button
        mButtonChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = BUSINESS_SEARCH;
                String location = mEditTextLocation.getText().toString();
                String priceRange = (String) mSpinnerPrice.getSelectedItem();

                // process url with multiple conditions
                url = url + "&location=" + location;
                int priceIndex=0;
                if (!priceRange.equals("no selection")){
                    if (priceRange.equals("$")){
                        priceIndex = 1;
                    } else if (priceRange.equals("$$")){
                        priceIndex = 2;
                    } else if (priceRange.equals("$$$")){
                        priceIndex = 3;
                    } else if (priceRange.equals("$$$$")){
                        priceIndex = 4;
                    }
                    url = url + "&price=" + priceIndex;
                }

                boolean openNowOption = mSwitchOpenNow.isChecked();

                if (openNowOption){
                    url = url + "&open_now=true";
                }

                new getRestaurantTask().execute(url);

                PrefsMgr.setString(MainActivity.this, "Location", location);
                PrefsMgr.setBoolean(MainActivity.this, "OpenNow", openNowOption);
                PrefsMgr.setInt(MainActivity.this, "Price", findPositionGivenPrice( priceRange, mPrices));

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class getRestaurantTask extends AsyncTask<String, Void, JSONObject> {
        private ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle("Fetching Result...");
            progressDialog.setMessage("One moment please...");
            progressDialog.setCancelable(true);
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                    "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getRestaurantTask.this.cancel(true);
                            progressDialog.dismiss();
                        }
                    });
            progressDialog.show();
        }
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                return JsonParser.fetchJson(params[0]);
            } catch (Exception e) {
                return null;
            }
        }
        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if (jsonObject == null){
                    throw new JSONException("no data available.");
                }

                // get all restaurant ids with multiple conditions
                JSONArray restaurants = jsonObject.getJSONArray("businesses");
                ArrayList<String> restaurantsFiltered = new ArrayList<>();
                boolean takeOutOption = mSwitchTakeOut.isChecked();
                boolean deliveryOption = mSwitchDelivery.isChecked();

                PrefsMgr.setBoolean(MainActivity.this, "TakeOut", takeOutOption);
                PrefsMgr.setBoolean(MainActivity.this, "Delivery", deliveryOption);


                for (int i =0; i<restaurants.length(); i++){

                    JSONObject restaurant = restaurants.getJSONObject(i);
                    ArrayList<String> transactions = new ArrayList<>();
                    JSONArray transactionsJSON = restaurant.getJSONArray("transactions");

                    for (int j=0; j<transactionsJSON.length(); j++){
                        transactions.add(transactionsJSON.getString(j));
                    }

                    if (takeOutOption && deliveryOption){
                        if (transactions.contains("pickup") && transactions.contains("delivery")){
                            restaurantsFiltered.add(restaurant.getString("id"));
                        }
                    } else if (takeOutOption){
                        if (transactions.contains("pickup")){
                            restaurantsFiltered.add(restaurant.getString("id"));
                        }
                    } else if (deliveryOption){
                        if (transactions.contains("delivery")){
                            restaurantsFiltered.add(restaurant.getString("id"));
                        }

                    } else {
                        restaurantsFiltered.add(restaurant.getString("id"));
                    }

                }


                // start new intent, result activity, pass restaurant id list
                Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                intent.putExtra(RESTAURANT_LIST, restaurantsFiltered);
                startActivity(intent);
                finish();

            } catch (JSONException e) {
                Toast.makeText(
                        MainActivity.this,
                        "There's been a JSON exception: " + e.getMessage(),
                        Toast.LENGTH_LONG
                ).show();
                e.printStackTrace();
            }
            progressDialog.dismiss();


        }
    }

    private int findPositionGivenPrice(String price, String[] mPrices) {
        for (int i = 0; i < mPrices.length; i++) {
            if (mPrices[i].equals(price)) {
                return i;
            }
        }
        //default
        return 0;
    }
}
